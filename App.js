import React from 'react';

// Use Navigation Ver. 5.7.0
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';

// Screen
import HomePage from './src/screens/homePage/HomePage';
import AddMoneyExpanse from './src/screens/addMoneyExpanse/AddMoneyExpanse';

const Stack = createStackNavigator();

function StackNav(props) {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="HomePage" headerMode={'none'}>
        <Stack.Screen name="HomePage" component={HomePage} />
        <Stack.Screen name="AddMoneyExpanse" component={AddMoneyExpanse} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default StackNav;
